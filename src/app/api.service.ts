import { Injectable } from '@angular/core';
import { Observable, of, throwError, Observer } from "rxjs";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { catchError, tap, map } from "rxjs/operators";
import { Product } from "./product";

//const Variables
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiURL = "/api/v1/products";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  //CRUD Endpoints
  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(apiURL)
               .pipe(
                 tap(product => console.log('fetched products')),
                 catchError(this.handleError('getProducts', []))
               );
  }

  getProduct(id: number): Observable<Product> {
    const url = `${apiURL}/${id}`;
    return this.http.get<Product>(url).pipe(
      tap(_ => console.log(`Fetched product id=${id}`)),
      catchError(this.handleError<Product>(`getProduct id=${id}`))
    );
  }

  addProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(apiURL, product, httpOptions).pipe(
      tap((prod: Product) => console.log(`added product with id=${product._id}`)),
      catchError(this.handleError<Product>('addProduct'))
    );
  }

  updateProduct(id: any, product: Product): Observable<any> {
    const url = `${apiURL}/${id}`;
    return this.http.put(url, product, httpOptions).pipe(
      tap(_ => console.log(`updated product by id: ${id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  deleteProduct(id: number): Observable<Product> {
    const url = `${apiURL}/${id}`;
    return this.http.delete<Product>(url, httpOptions).pipe(
      tap(_ => console.log(`Deleted product by id: ${id}`)),
      catchError(this.handleError<Product>('deleteProduct'))
    );
  }

  //Error Handling
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}
