export class Product {
    _id: string;
    prod_name: string;
    prod_desc: string;
    prod_price: string;
    updated_at: Date;
}