import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from "../api.service";
import { Product } from "../product";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  //Material Table Data Source
  displayedColumns: string[] = ['prod_name','prod_price'];
  data: Product[] = [];
  isLoadingResults = true;

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.loadProducts();
  }

  loadProducts(){
    this.api.getProducts()
        .subscribe((res: any) => {
          this.data = res;
          console.log(this.data);
          this.isLoadingResults = false;
        }, err => {
          console.log(err);
          this.isLoadingResults = false;
        })
  }

}
